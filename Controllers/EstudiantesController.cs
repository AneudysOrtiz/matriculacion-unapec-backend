using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using matriculacion.Data;
using matriculacion.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace matriculacion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstudiantesController : ControllerBase
    {
        private readonly MatriculacionContext _db;
        public EstudiantesController(MatriculacionContext db)
        {
            this._db = db;
        }

        // GET api/values/5
        [HttpGet("[action]")]
        public ActionResult<Estudiante> Search([FromQuery]string cedula)
        {
            return Ok(_db.Estudiantes.Include(y => y.HistorialEstudiante)
            .ThenInclude(x => x.Materia)
            .FirstOrDefault(x => x.Cedula == cedula.Replace("-", "") || x.Matricula == cedula.Replace("-", "")));
        }
    }
}
