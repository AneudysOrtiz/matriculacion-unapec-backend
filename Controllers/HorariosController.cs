using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using matriculacion.Data;
using matriculacion.Dto;
using matriculacion.Models;
using matriculacion.MyHub;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace matriculacion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HorariosController : ControllerBase
    {
        private readonly MatriculacionContext _db;
        private readonly IHubContext<HorariosHub> _hub;
        public HorariosController(MatriculacionContext db, IHubContext<HorariosHub> hub)
        {
            this._db = db;
            this._hub = hub;
        }
        // GET api/horarios
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var horarios = _db.Horarios.ToList();
            await _hub.Clients.All.SendAsync("disponibles", horarios);
            return Ok(new { message = "Sent" });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Horario>> Get(int id)
        {
            return _db.Horarios.Where(x => x.IdMateria == id).ToList();
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] HorarioDto dto)
        {
            var horario = _db.Horarios.Find(dto.IdHorario);
            if (horario == null)
                return BadRequest(new { message = "Horario Invalido" });

            if (!dto.Inscribir)
                horario.Cupo += 1;

            else if (dto.Inscribir && horario.Cupo > 0)
                horario.Cupo -= 1;

            else
                return BadRequest(new { message = "No hay cupo disponible" });

            _db.SaveChanges();
            var horarios = _db.Horarios.ToList();
            await _hub.Clients.All.SendAsync("disponibles", horarios);
            return Ok();
        }
    }
}
