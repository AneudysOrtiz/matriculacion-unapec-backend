using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using matriculacion.Data;
using matriculacion.Models;
using Microsoft.AspNetCore.Mvc;

namespace matriculacion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriasController : ControllerBase
    {
        private readonly MatriculacionContext _db;
        public MateriasController(MatriculacionContext db)
        {
            this._db = db;
        }
        // GET api/horarios
        [HttpGet]
        public ActionResult Get()
        {
            var materias = _db.Materias.GroupBy(x => x.IdCuatrimestre).Select(a => new { Cuatrimestre = a.Key, Materias = a });
            return Ok(materias);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Materia> Get(int id)
        {
            return _db.Materias.Find(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
