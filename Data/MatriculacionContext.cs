using matriculacion.Models;
using Microsoft.EntityFrameworkCore;

namespace matriculacion.Data
{
    public class MatriculacionContext : DbContext
    {
        public MatriculacionContext(DbContextOptions options) : base(options)
        {

        }
        public virtual DbSet<Materia> Materias { get; set; }
        public virtual DbSet<Horario> Horarios { get; set; }
        public virtual DbSet<Estudiante> Estudiantes { get; set; }
        public virtual DbSet<HistorialEstudiante> HistorialEstudiantes { get; set; }
    }
}