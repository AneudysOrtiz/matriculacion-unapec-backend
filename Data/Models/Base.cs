using System.ComponentModel.DataAnnotations;

namespace matriculacion.Models
{
    public class Base
    {
        [Key]
        public int Id { get; set; }
    }
}