using System;
using System.Collections.Generic;
using matriculacion.Models;

namespace matriculacion.Data
{
    public class Estudiante : Base
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Cedula { get; set; }
        public string Matricula { get; set; }
        public string Centro { get; set; }
        public virtual ICollection<HistorialEstudiante> HistorialEstudiante { get; set; }
    }
}