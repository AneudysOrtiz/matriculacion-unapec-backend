using matriculacion.Models;

namespace matriculacion.Data
{
    public class HistorialEstudiante : Base
    {
        public int MateriaId { get; set; }
        public virtual Materia Materia { get; set; }
        public int EstudianteId { get; set; }
        public virtual Estudiante Estudiante { get; set; }
    }
}