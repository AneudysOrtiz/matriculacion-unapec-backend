namespace matriculacion.Models
{
    public class Horario : Base
    {
        public int IdMateria { get; set; }
        public bool Estado { get; set; }
        public string Aula { get; set; }
        public string Grupo { get; set; }
        public string Lun { get; set; }
        public string Mar { get; set; }
        public string Mier { get; set; }
        public string Jue { get; set; }
        public string Vie { get; set; }
        public string Sab { get; set; }
        public string Dom { get; set; }
        public int Modulo { get; set; }
        public int Cupo { get; set; }
    }
}