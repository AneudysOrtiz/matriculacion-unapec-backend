namespace matriculacion.Models
{
    public class Materia : Base
    {
        public int IdCuatrimestre { get; set; }
        public bool Estado { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Creditos { get; set; }
    }
}