namespace matriculacion.Dto
{
    public class HorarioDto
    {
        public int IdHorario { get; set; }
        public bool Inscribir { get; set; }
    }
}