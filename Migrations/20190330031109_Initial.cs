﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace matriculacion.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Horarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdMateria = table.Column<int>(nullable: false),
                    Estado = table.Column<bool>(nullable: false),
                    Aula = table.Column<string>(nullable: true),
                    Grupo = table.Column<string>(nullable: true),
                    Lun = table.Column<string>(nullable: true),
                    Mar = table.Column<string>(nullable: true),
                    Mier = table.Column<string>(nullable: true),
                    Jue = table.Column<string>(nullable: true),
                    Vie = table.Column<string>(nullable: true),
                    Sab = table.Column<string>(nullable: true),
                    Dom = table.Column<string>(nullable: true),
                    Modulo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Horarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Materias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCuatrimestre = table.Column<int>(nullable: false),
                    Estado = table.Column<bool>(nullable: false),
                    Codigo = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Creditos = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materias", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Horarios");

            migrationBuilder.DropTable(
                name: "Materias");
        }
    }
}
