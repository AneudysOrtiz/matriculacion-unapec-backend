﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace matriculacion.Migrations
{
    public partial class cedula : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RNE",
                table: "Estudiantes",
                newName: "Cedula");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Cedula",
                table: "Estudiantes",
                newName: "RNE");
        }
    }
}
