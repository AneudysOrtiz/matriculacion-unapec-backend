﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace matriculacion.Migrations
{
    public partial class historialEstudiante : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Duracion",
                table: "HistorialEstudiantes");

            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "HistorialEstudiantes");

            migrationBuilder.DropColumn(
                name: "TituloObtenido",
                table: "HistorialEstudiantes");

            migrationBuilder.AddColumn<int>(
                name: "MateriaId",
                table: "HistorialEstudiantes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_HistorialEstudiantes_MateriaId",
                table: "HistorialEstudiantes",
                column: "MateriaId");

            migrationBuilder.AddForeignKey(
                name: "FK_HistorialEstudiantes_Materias_MateriaId",
                table: "HistorialEstudiantes",
                column: "MateriaId",
                principalTable: "Materias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HistorialEstudiantes_Materias_MateriaId",
                table: "HistorialEstudiantes");

            migrationBuilder.DropIndex(
                name: "IX_HistorialEstudiantes_MateriaId",
                table: "HistorialEstudiantes");

            migrationBuilder.DropColumn(
                name: "MateriaId",
                table: "HistorialEstudiantes");

            migrationBuilder.AddColumn<string>(
                name: "Duracion",
                table: "HistorialEstudiantes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "HistorialEstudiantes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TituloObtenido",
                table: "HistorialEstudiantes",
                nullable: true);
        }
    }
}
