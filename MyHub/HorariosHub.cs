using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace matriculacion.MyHub
{
    public class HorariosHub : Hub
    {
        public async Task JoinGroup(string groupName)
        {
            
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            await Clients.Group(groupName).SendAsync("JoinGroup", groupName);
        }
    }
}